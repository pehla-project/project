package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

// Database
public class Datasource {
    public static final String DB_NAME = "music.db";

    public static final String CONNECTION_STRING = "jdbc:sqlite:D:\\Users\\Ritik_saini\\IdeaProjects\\Music\\" + DB_NAME;

    public static final String TABLE_ALBUMS = "albums";
    public static final String COLUMN_ALBUM_ID = "_id";
    public static final String COLUMN_ALBUM_NAME = "name";
    public static final String COLUMN_ALBUM_ARTIST = "artist";
    public static final int INDEX_ALBUMS_ID = 1;
    public static final int INDEX_ALBUMS_NAME = 2;
    public static final int INDEX_ALBUMS_ARTIST = 3;

    public static final String TABLE_ARTISTS = "artists";
    public static final String COLUMN_ARTIST_ID = "_id";
    public static final String COLUMN_ARTIST_NAME = "name";
    public static final int INDEX_ARTISTS_ID = 1;
    public static final int INDEX_ARTISTS_NAME = 2;


    public static final String TABLE_SONGS = "songs";
    public static final String COLUMN_SONG_ID = "_id";
    public static final String COLUMN_SONG_TRACK = "track";
    public static final String COLUMN_SONG_TITLE = "title";
    public static final String COLUMN_SONG_ALBUM = "album";
    public static final int INDEX_SONGS_ID = 1;
    public static final int INDEX_SONGS_TRACK = 2;
    public static final int INDEX_SONGS_TITLE = 3;
    public static final int INDEX_SONGS_ALBUM = 4;

    public static final int ORDER_BY_NONE = 1;
    public static final int ORDER_BY_ASC = 2;
    public static final int ORDER_BY_DESC = 3;

    //  SELECT albums.name FROM albums INNER JOIN
    //  artists ON albums.artist = artists._id
    //  WHERE artists.name = "Carole King"
    //ORDER BY albums.name COLLATE NOCASE ASC;
    public static final String QUERY_ALBUMS_BY_ARTIST_START =
            "SELECT " + TABLE_ALBUMS + '.' + COLUMN_ALBUM_NAME + " FROM " + TABLE_ALBUMS
                    + " INNER JOIN " + TABLE_ARTISTS + " ON " + TABLE_ALBUMS + "." + COLUMN_ALBUM_ARTIST
                    + " = " + TABLE_ARTISTS + "." + COLUMN_ARTIST_ID + " WHERE " + TABLE_ARTISTS + "." + COLUMN_ARTIST_NAME
                    + " = \"";
    public static final String QUERY_ALBUMS_BY_ARTIST_SORT =
            " ORDER BY " + TABLE_ALBUMS + "." + COLUMN_ALBUM_NAME + " COLLATE NOCASE ";


    //    SELECT artists.name, albums.name, songs.track
//    FROM songs INNER JOIN albums ON songs.album = albums._id
//    INNER JOIN artists ON albums.artist = artists._id
//    WHERE songs.title = "Go Your Own Way"
//    ORDER BY artists.name, albums.name
//    COLLATE NOCASE ASC
    public static final String QUERY_ARTIST_FOR_SONG_START =
            "SELECT " + TABLE_ARTISTS + "." + COLUMN_ARTIST_NAME + ", " + TABLE_ALBUMS
                    + "." + COLUMN_ALBUM_NAME + ", " + TABLE_SONGS + "." + COLUMN_SONG_TRACK
                    + " FROM " + TABLE_SONGS + " INNER JOIN " + TABLE_ALBUMS + " ON " + TABLE_SONGS
                    + "." + COLUMN_SONG_ALBUM + " = " + TABLE_ALBUMS + "." + COLUMN_ALBUM_ID + " INNER JOIN "
                    + TABLE_ARTISTS + " ON " + TABLE_ALBUMS + "." + COLUMN_ALBUM_ARTIST + " = "
                    + TABLE_ARTISTS + "." + COLUMN_ARTIST_ID + " WHERE " + TABLE_SONGS + "." + COLUMN_SONG_TITLE
                    + " = \"";

    public static final String QUERY_ARTIST_FOR_SONG_SORT =
            " ORDER BY " + TABLE_ARTISTS + "." + COLUMN_ARTIST_NAME + ", " + TABLE_ALBUMS + "." + COLUMN_ALBUM_NAME
                    + " COLLATE NOCASE ";

    //    CREATE VIEW IF NOT EXISTS artist_list AS SELECT artists.name, albums.name AS album,
//    songs.track, songs.title FROM songs INNER JOIN albums ON songs.album = albums._id
//    INNER JOIN artists ON albums.artist = artist._id ORDER BY artists.name,
//    albums.name, songs.track
    public static final String TABLE_ARTIST_SONG_VIEW = "artist_list";

    public static final String CREATE_ARTIST_FOR_SONG_VIEW = "CREATE VIEW IF NOT EXISTS " + TABLE_ARTIST_SONG_VIEW +
            " AS " + " SELECT " + TABLE_ARTISTS + "." + COLUMN_ARTIST_NAME + ", " + TABLE_ALBUMS + "." + COLUMN_ALBUM_NAME
            + " AS " + " album " + ", " + TABLE_SONGS + "." + COLUMN_SONG_TRACK + ", " + TABLE_SONGS + "." + COLUMN_SONG_TITLE
            + " FROM " + TABLE_SONGS + " INNER JOIN " + TABLE_ALBUMS + " ON " + TABLE_SONGS + "." + COLUMN_SONG_ALBUM + " = "
            + TABLE_ALBUMS + "." + COLUMN_ALBUM_ID + " INNER JOIN " + TABLE_ARTISTS + " ON " + TABLE_ALBUMS + "." + COLUMN_ALBUM_ARTIST
            + " = " + TABLE_ARTISTS + "." + COLUMN_ARTIST_ID + " ORDER BY " + TABLE_ARTISTS + "." + COLUMN_ARTIST_NAME
            + ", " + TABLE_ALBUMS + "." + COLUMN_ALBUM_NAME + ", " + TABLE_SONGS + "." + COLUMN_SONG_TRACK;

    //    SELECT name,album,track FROM artist_list WHERE title = "Go Your Own Way";
    public static final String QUERY_VIEW_SONG_INFO = "SELECT " + COLUMN_ARTIST_NAME + ", "
            + COLUMN_SONG_ALBUM + ", " + COLUMN_SONG_TRACK + " FROM " + TABLE_ARTIST_SONG_VIEW
            + " WHERE " + COLUMN_SONG_TITLE + " = ";

    public static final String QUERY_VIEW_SONG_INFO_PREPARED = "SELECT " + COLUMN_ARTIST_NAME + ", "
            + COLUMN_SONG_ALBUM + ", " + COLUMN_SONG_TRACK + " FROM " + TABLE_ARTIST_SONG_VIEW
            + " WHERE " + COLUMN_SONG_TITLE + " = ?";


    // INSERT INTO artist(name) values();
    public static final String INSERT_ARTIST = "INSERT INTO " + TABLE_ARTISTS +
            "(" + COLUMN_ARTIST_NAME + ") VALUES (?)";

    // INSERT INTO albums(name,artist) values();
    public static final String INSERT_ALBUMS = "INSERT INTO " + TABLE_ALBUMS +
            "(" + COLUMN_ALBUM_NAME + ", " + COLUMN_ALBUM_ARTIST + ") VALUES(?, ?)";


    // INSERT INTO songs(track,title,album) values();
    public static final String INSERT_SONGS = "INSERT INTO " + TABLE_SONGS +
            "(" + COLUMN_SONG_TRACK + ", " + COLUMN_SONG_TITLE + ", " + COLUMN_SONG_ALBUM
            + ") VALUES( ?, ?, ?)";

    // QUERY THE ARTIST FOR ARTIST_ID
    public static final String QUERY_ARTIST = "SELECT " + COLUMN_ARTIST_ID + " FROM " +
            TABLE_ARTISTS + " WHERE " + COLUMN_ARTIST_NAME + " = ?";

    // QUERY THE ALBUM FOR ALBUM_ID
    public static final String QUERY_ALBUM = "SELECT " + COLUMN_ALBUM_ID + " FROM " +
            TABLE_ALBUMS + " WHERE " + COLUMN_ALBUM_NAME + " = ?";

    // QUERY THE SONG FOR THE SONG_ID
    public static final String QUERY_SONG = "SELECT " + COLUMN_SONG_ID + " FROM " +
            TABLE_SONGS + " WHERE " + COLUMN_SONG_TITLE + " = ?";

    private Connection conn;

    // We want an Instance variable because we only want to create it once we don't want to create it every time we query
    // because we only want to precompiled once.
    private PreparedStatement querySongInfoView;

    private PreparedStatement insertIntoArtist;
    private PreparedStatement insertIntoAlbums;
    private PreparedStatement insertIntoSongs;

    private PreparedStatement queryArtist;
    private PreparedStatement queryAlbum;
    private PreparedStatement querySong;

    public boolean open() {
        try {
            conn = DriverManager.getConnection(CONNECTION_STRING);
            querySongInfoView = conn.prepareStatement(QUERY_VIEW_SONG_INFO_PREPARED);
            insertIntoArtist = conn.prepareStatement(INSERT_ARTIST, Statement.RETURN_GENERATED_KEYS);
            insertIntoAlbums = conn.prepareStatement(INSERT_ALBUMS, Statement.RETURN_GENERATED_KEYS);
            insertIntoSongs = conn.prepareStatement(INSERT_SONGS);


            queryArtist = conn.prepareStatement(QUERY_ARTIST);
            queryAlbum = conn.prepareStatement(QUERY_ALBUM);
            querySong = conn.prepareStatement(QUERY_SONG);

            return true;
        } catch (SQLException e) {
            System.out.println("Couldn't connect to database: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public void close() {
        try {

            if (querySongInfoView != null) {
                querySongInfoView.close();
            }
            if (insertIntoArtist != null) {
                insertIntoArtist.close();
            }
            if (insertIntoAlbums != null) {
                insertIntoAlbums.close();
            }
            if (insertIntoSongs != null) {
                insertIntoSongs.close();
            }
            if (queryArtist != null) {
                queryArtist.close();
            }
            if (queryAlbum != null) {
                queryAlbum.close();
            }
            if (querySong != null){
                querySong.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("Couldn't close connection: " + e.getMessage());
        }
    }

    public List<Artist> queryArtists(int sortOrder) {


        // CREATING THE SQL QUERY STATEMENT AND STORING IT IN THE sb OBJECT.
        StringBuilder sb = new StringBuilder("SELECT * FROM ");
        sb.append(TABLE_ARTISTS);

        // checking to see if sortOrder IS NOT EQUAL TO 1
        if (sortOrder != ORDER_BY_NONE) {
            sb.append(" ORDER BY ");
            sb.append(COLUMN_ARTIST_NAME);
            sb.append(" COLLATE NOCASE ");

            if (sortOrder == ORDER_BY_DESC) {
                sb.append("DESC");
            } else {
                sb.append("ASC");
            }
        }
        try (Statement statement = conn.createStatement();
             // ResultSet object will contain all the data based on the query in the table form.
             ResultSet results = statement.executeQuery(sb.toString())) {

            List<Artist> artists = new ArrayList<>();

            while (results.next()) {
                Artist artist = new Artist();
                //The ResultSet interface provides getter methods (getBoolean, getLong, and so on) for retrieving
                // column values from the current row.
                // Values can be retrieved using either the index number of the column or the name of the column.
                artist.setId(results.getInt(INDEX_ARTISTS_ID));
                artist.setName(results.getString(INDEX_ARTISTS_NAME));

                // Storing this object in the artists list.
                artists.add(artist);
            }

            return artists;
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
            return null;
        }
    }


    public List<String> queryAlbumsForArtist(String artistName, int sortOrder) {

        StringBuilder sb = new StringBuilder(QUERY_ALBUMS_BY_ARTIST_START);
        sb.append(artistName);
        sb.append("\"");

        if (sortOrder != ORDER_BY_NONE) {
            sb.append(QUERY_ALBUMS_BY_ARTIST_SORT);

            if (sortOrder == ORDER_BY_DESC) {
                sb.append("DESC");
            } else {
                sb.append("ASC");
            }
        }

        System.out.println("SQL STATEMENT = " + sb.toString());

        try (Statement statement = conn.createStatement();
             ResultSet results = statement.executeQuery(sb.toString())) {

            //This will store the StringName of the above executed query in the albums list.

            List<String> albums = new ArrayList<>();
            while (results.next()) {
                // Storing this object in the albums list.
                albums.add(results.getString(1));
            }
            return albums;
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
            return null;
        }
    }


    public List<SongArtist> queryArtistForSong(String songName, int sortOrder) {
        StringBuilder sb = new StringBuilder(QUERY_ARTIST_FOR_SONG_START);
        sb.append(songName);
        sb.append("\"");

        if (sortOrder != ORDER_BY_NONE) {
            sb.append(QUERY_ARTIST_FOR_SONG_SORT);
            if (sortOrder == ORDER_BY_DESC) {
                sb.append("ASC");
            } else {
                sb.append("DESC");
            }
        }

        System.out.println("SQL STATEMENT: " + sb.toString());

        try (Statement statement = conn.createStatement();
             ResultSet results = statement.executeQuery(sb.toString())) {

            List<SongArtist> artists = new ArrayList<>();

            while (results.next()) {
                SongArtist artist = new SongArtist();
                artist.setArtistName(results.getString(1));
                artist.setAlbumName(results.getString(2));
                artist.setTrack(results.getInt(3));

                // Storing this object in the artists list.
                artists.add(artist);
            }
            return artists;
        } catch (SQLException e) {
            System.out.println("QUERY Failed: " + e.getMessage());
            return null;
        }
    }


    public void querySongMetadata() {
        String sql = "SELECT * FROM " + TABLE_SONGS;

        try (Statement statement = conn.createStatement();
             ResultSet results = statement.executeQuery(sql)) {

            ResultSetMetaData meta = results.getMetaData();
            int numColumns = meta.getColumnCount();

            for (int i = 1; i <= numColumns; i++) {
                System.out.format("Column %d in the songs table is names %s\n",
                        i, meta.getColumnName(i));
            }
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
        }
    }


    public int getCount(String table) {
        String sql = "SELECT COUNT(*) AS count FROM " + table;

        try (Statement statement = conn.createStatement();
             ResultSet results = statement.executeQuery(sql)) {

            int count = results.getInt("count");

            System.out.format("Count = %d\n", count);
            return count;
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
            return -1;

        }
    }


    public boolean createViewForSongArtists() {

        System.out.println("SQL QUERY " + CREATE_ARTIST_FOR_SONG_VIEW.toString());

        try (Statement statement = conn.createStatement()) {
            statement.execute(CREATE_ARTIST_FOR_SONG_VIEW);
            return true;
        } catch (SQLException e) {
            System.out.println("Query failed : " + e.getMessage());
            return false;
        }
    }


    public List<SongArtist> queryViewSongInfo(String titleName) {


        try {
            querySongInfoView.setString(1, titleName);
            ResultSet results = querySongInfoView.executeQuery();


            //Creating the list of artists. This list will hold the SongArtist objects.
            List<SongArtist> artists = new ArrayList<>();

            while (results.next()) {
                SongArtist artist = new SongArtist();
                artist.setArtistName(results.getString(1));
                artist.setAlbumName(results.getString(2));
                artist.setTrack(results.getInt(3));

                // Storing this object in the artists list.
                artists.add(artist);
            }
            return artists;
        } catch (SQLException e) {
            System.out.println("Query Failed: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    private int insertArtist(String name) throws SQLException {

        // Setting up the name in the query,this will jump on the line 112 and
        // set the name after the =
        queryArtist.setString(1, name);
        // We are checking to see here Weather the artist is already exists or not
        ResultSet results = queryArtist.executeQuery();

        if (results.next()) {
            return results.getInt(1);
        } else {

            // This will add the artist in the database.
            insertIntoArtist.setString(1, name);
            int affectedRows = insertIntoArtist.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Couldn't insert the artist !");
            }

            ResultSet generatedKeys = insertIntoArtist.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);

            } else {
                throw new SQLException("Couldn't get the generated keys for artists!");
            }
        }
    }

    private int insertAlbums(String name, int artistId) throws SQLException {

        // Setting up the name in the query,this will jump on the line 112 and
        // set the name after the =
        queryAlbum.setString(1, name);
        // We are checking to see here Weather the artist is already exists or not
        ResultSet results = queryAlbum.executeQuery();


        if (results.next()) {
            return results.getInt(1);
        } else {

            // This will add the artist in the database.
            insertIntoAlbums.setString(1, name);
            insertIntoAlbums.setInt(2, artistId);
            int affectedRows = insertIntoAlbums.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Couldn't insert the albums !");
            }

            ResultSet generatedKeys = insertIntoAlbums.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);

            } else {
                throw new SQLException("Couldn't get the generated keys for albums!");
            }
        }
    }

    public int insertSongs(String title, String artist, String album, int track) throws SQLException {

            querySong.setString(1,title);
            ResultSet results = querySong.executeQuery();

            if(results.next()){
                return results.getInt(1);
            }else {
                try {

                    // this two statement will call the insertArtist method to insert artist
                    // and insert albums to insert the album and after that we can insert the songs

                    int artistId = insertArtist(artist);
                    int albumId = insertAlbums(album, artistId);

                    insertIntoSongs.setInt(1, track);
                    insertIntoSongs.setString(2, title);
                    insertIntoSongs.setInt(3, albumId);

                    int affectedRows = insertIntoSongs.executeUpdate();

                    if (affectedRows == 1) {
                        conn.commit();
                    } else {
                        throw new SQLException("The song insertion failed.");
                    }
                } catch (SQLException e) {
                    System.out.println("Insert song exception: " + e.getMessage());
                    e.printStackTrace();

                    try {
                        System.out.println("Performing rollback");
                        conn.rollback();

                    } catch (SQLException e2) {
                        System.out.println("Oh boy! Things are really bad! " + e2.getMessage());
                    }
                } finally {
                    try {
                        System.out.println("Resetting default commit behavior");
                        conn.setAutoCommit(true);

                    } catch (SQLException e) {
                        System.out.println("Couldn't reset auto commit" + e.getMessage());
                    }
                }
            }
        return -1;
    }
}

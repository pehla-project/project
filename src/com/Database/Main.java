package com.Database;

import model.Artist;
import model.Datasource;
import model.SongArtist;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {


        Datasource datasource = new Datasource();
        if (!datasource.open()) {
            System.out.println("Can't open datasource");
            return;
        }


//        List<Artist> artists = datasource.queryArtists(Datasource.ORDER_BY_ASC);
//        if(artists==null){
//            System.out.println("No artists!");
//            return;
//        }
//
//        for(Artist artist: artists){
//            System.out.println("ID = " + artist.getId() + ", Name = " + artist.getName());
//
//        }
//
//        List<String> albumsForArtist
//                = datasource.queryAlbumsForArtist("Aerosmith", Datasource.ORDER_BY_ASC);
//
//        for(String album:albumsForArtist){
//            System.out.println(album);
//        }
//
//
//        List<SongArtist> songArtists
//                = datasource.queryArtistForSong("She's On Fire", Datasource.ORDER_BY_ASC);
//
//        for(SongArtist artist:songArtists){
//            System.out.println("Artist Name :" + artist.getArtistName()
//            + " ,Album Name : " + artist.getAlbumName() + " ,Track ID : " + artist.getTrack());
//        }

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Enter a song title: ");
//        String title = scanner.nextLine();
//
//
//        List<SongArtist> artists = datasource.queryViewSongInfo(title);
//
//        for(SongArtist artist:artists){
//            System.out.println("Artist Name: " + artist.getArtistName() + ", Album Name : "
//                    + artist.getAlbumName() + ", " + "Track Number: " + artist.getTrack());
//        }

//        datasource.getCount(Datasource.TABLE_ARTISTS);
//        datasource.createViewForSongArtists();
//        datasource.querySongMetadata();

        try{
            datasource.insertSongs("Blah Blah Blah", "Armin Van", "ASOT",1);
        }catch(SQLException e){
            System.out.println("The song is already present on the database");
        }

//        datasource.insertSongs("")

//        System.out.println("Main method Called");
//        SIBTest test = new SIBTest();
//        test.someMethod();
//        System.out.println("Owner is " + SIBTest.owner);
        datasource.close();
    }
}

